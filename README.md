# Communication PC-Arduino

## Prérequis
Un environnement Python contenant les librairies `pyserial` et `matplotlib` 

## Appairage module bluetooth
Se connecter au module bluetooth via l'OS en rentrant le code de connexion du module.
### Mac OS
Changer la ligne 88 : 
```python
port = '/dev/tty.<NOM_DU_DEVICE_BLUETOOTH>'
```
### Linux
Après s'être connecté, binder le module sur un port en lançant la commande
```
sudo rfcomm bind 0 "<ADRESSE_MAC>" 1
```
Ensuite changer la ligne 88:
```python
port = '/dev/rfcomm0'
```
### Windows
Pas de compatibilité assurée

## Lancer l'application 
Lancer le script python `script.py`

## Après l'utilisation de l'application
Sur **Linux**, il faut unbind le module de `/dev/`
```
sudo rfcomm unbind 0 rfcomm0
```