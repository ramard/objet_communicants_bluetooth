import serial
import matplotlib.pyplot as plt


class INFOS:
    TEMPERATURE = 'T'
    LIGHT = 'L'
    HUMIDITY = 'H'


class Action:
    READ = 'R'
    WRITE = 'W'
    READ_ALL = 'RA'


class Message:
    # Exemple : "S;2;L;10;24;28;45;...;34"
    STATUS = 0
    ID_GREENHOUSE = 1
    INFO = 2
    NB_INFOS = 3
    START = 4


class Status:
    SUCCESS = 'S'
    FAIL = 'F'


class Colors:
    HEADER = '\033[93m'
    SUCCESS = '\033[92m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


information_title = {
    'T': "Température",
    'L': "Lumière",
    'H': "Humidité",
}


def read_request(bluetooth, info):
    request = str(Action.READ + ";" + info)
    bluetooth.write(str.encode(request))


def write_request(bluetooth, info, id_greenhouse, val):
    request = str(
        Action.WRITE
        + ";" +
        str(id_greenhouse)
        + ";" +
        str(info)
        + ";" +
        str(val)
    )
    bluetooth.write(str.encode(request))


def read_response(bluetooth):
    bluetooth.flushInput()
    input_data = bluetooth.readline().decode("utf-8")
    result = []
    while input_data != "END\r\n":
        print(input_data)
        bluetooth.flushInput()
        result.append(input_data)
        bluetooth.flushInput()
        bluetooth.write(str.encode("ACK"))
        input_data = bluetooth.readline().decode("utf-8")
    return result


def write_response(bluetooth):
    bluetooth.flushInput()
    input_data = bluetooth.readline().decode("utf-8")
    return input_data


def connexion():
    port = "/dev/tty.H-C-2010-06-01"
    bluetooth = serial.Serial(port, 38400)
    if bluetooth is not None:
        print("Connected")
    else:
        print("Not connected")
    return bluetooth


def deconnexion(bluetooth):
    bluetooth.close()


def process_read_response(response):
    result = []
    for string in response:
        result.append(string.replace("\r\n", "").split(";"))
    return result


def display_data(data_processed):
    for data in data_processed:
        if data[Message.STATUS] == Status.FAIL:
            print(Colors.FAIL + "La requête a échouée pour une serre" + Colors.END)
        else:
            nb_data = int(data[Message.NB_INFOS])
            id_greenhouse = int(data[Message.ID_GREENHOUSE])
            data_to_received = data[Message.START:Message.START + nb_data]
            data_to_display = list(map(int, data_to_received))

            plt.plot(data_to_display, label="Serre " + str(id_greenhouse))

    data_info = information_title[data_processed[0][Message.INFO]]
    title = "Informations récoltées : " + data_info
    plt.title(title)
    plt.xlabel("Nombre de mesures")
    plt.ylabel("Unités de la mesure effectuée")
    plt.legend()
    plt.show()


def read_information(info):
    bt = connexion()
    read_request(bt, info)
    data = read_response(bt)
    if data is not None and data != []:
        processed_data = process_read_response(data)
        display_data(processed_data)
    else:
        print("Pas de donnée reçu")


def write_informations(info, id_greenhouse, val):
    bt = connexion()
    write_request(bt, info, id_greenhouse, val)
    data = write_response(bt)
    print(data[Message.STATUS])
    if data[Message.STATUS] == Status.SUCCESS:
        print(Colors.SUCCESS + "Information correctement modifiée" + Colors.END)
    else:
        print(Colors.FAIL + "Un problème est survenu lors du changement de l'information" + Colors.END)


def display_intructions():
    print(Colors.HEADER)
    print("\n####################################################################")
    print(Colors.UNDERLINE + "Tapez le charactères associé à l'action souhaitée" + Colors.END + Colors.HEADER)
    print("— R : Consulter")
    print("— W : Editer ")
    print("— Q - Quitter")
    print(Colors.UNDERLINE + "Tapez le charactères associé à l'information souhaitée" + Colors.END + Colors.HEADER)
    for key, value in information_title.items():
        print("—", key, " : ", value)
    print("####################################################################")
    print(Colors.END)


def main():
    display_intructions()

    action = str.upper(input(Colors.HEADER + "Quelle action voulez vous faire ? \n" + Colors.END))
    while action != 'Q':
        if action == Action.READ or action == Action.WRITE:
            info = str.upper(input(Colors.HEADER + "Sur quelle information ? \n" + Colors.END))
            if info in information_title:
                if action == Action.READ:
                    read_information(info)
                else:
                    id_greenhouse = int(input(Colors.HEADER + "ID de la serre ? \n" + Colors.END))
                    val = int(input(Colors.HEADER + "Nouvelle valeur ? \n" + Colors.END))
                    write_informations(info, id_greenhouse, val)
            else:
                print("Information non reconnue \n")
        else:
            print("Action non reconnue \n")

        display_intructions()
        action = str.upper(input(Colors.HEADER + "Quelle action voulez vous faire ? \n" + Colors.END))


def simulate_data_received():
    data = [
        "S;1;L;4;24;28;45;34\r\n",
        "S;2;L;4;26;24;56;28\r\n",
        "S;3;L;4;45;54;34;8\r\n",
    ]
    processed_data = process_read_response(data)
    display_data(processed_data)


if __name__ == '__main__':
    main()

# %%
