#include "SoftwareSerial.h"

bool canResponse = true;
extern int TemperatureThreshold;
extern int LightThreshold;
extern int HumidityThreshold;

void response(String response){
  canResponse = false;
  Serial.println("[RESPONSE] "+response);
  BTSerie.println(response);
}

extern void manageAck(char *response){
  char** splitRequest = requestToArray(response);
  if(String(splitRequest[0]) == String(ACK)){
    canResponse = true; 
  }
}

extern void manageComputerRequest(char *request) {
  char** splitRequest = requestToArray(request);
  char requestResult;
  
  // LECTURE
  if(String(splitRequest[0])==String(READ)){
    uint8_t lenGreenhouseArray; // nombre de serres
    uint8_t *greenhouseArray; // liste d'id des serres
    uint8_t *lenRequestArray; // nombre de données pour la requête pour chaque serre
    uint8_t **requestArray; // matrice des données pour la requête
    char requestType;
    if(String(splitRequest[1]) == String(TEMPERATURE)){ // TODO : gérer ici la récupération des températures
      Serial.println("Temperature request"); 
      requestType = TEMPERATURE;
    }
    else if(String(splitRequest[1]) == String(LIGHT)){
      Serial.println("Light request");
      requestArray = getData(&lenGreenhouseArray, &greenhouseArray, &lenRequestArray, reportingLuminosite);
      requestType = LIGHT;
    }
    else if(String(splitRequest[1]) == String(HUMIDITY)){ 
      Serial.println("Humidity request");
      requestArray = getData(&lenGreenhouseArray, &greenhouseArray, &lenRequestArray, reportingHydrometrie);
      requestType = HUMIDITY;
    }


    for(uint8_t i=0; i<lenGreenhouseArray; i++){
      String stringRequestArray = toString(requestArray[i], nbReport);
      response(String(SUCCESS) + ";" + String(greenhouseArray[i]) + ";" + String(requestType) + ";" + String(nbReport) + ";" + stringRequestArray);
      // on attend d'avoir reçu la confirmation de lecture avant d'envoyer la suite 
      // sinon le pc qui écoute peut ne pas tout recevoir
      while(!canResponse){
        listen(manageAck);
      } 
    }
    response(END);
  }
  // ECRITURE
  else if (String(splitRequest[0])== String(WRITE)){
    char responseResult;
    if(String(splitRequest[2]) == String(LIGHT)){
      Serial.println("Light setting");
      configTab[String(splitRequest[1]).toInt()][reportingLuminosite] = String(splitRequest[3]).toInt();
      responseResult = SUCCESS;
    }
    else if(String(splitRequest[2]) == String(HUMIDITY)){
      Serial.println("Humidity setting");
      configTab[String(splitRequest[1]).toInt()][reportingHydrometrie] = String(splitRequest[3]).toInt();
      responseResult = SUCCESS; 
    }
    else if(String(splitRequest[2]) == String(TEMPERATURE)){
      Serial.println("Temperature setting");
      configTab[String(splitRequest[1]).toInt()][reportingTemperature] = String(splitRequest[3]).toInt();
      responseResult = SUCCESS;
    }
    else{
      responseResult = FAILURE;
    }

    while(!canResponse){
      listen(manageAck);
    } 
    
    response(String(responseResult));
  }

  canResponse = true;
}

// decompose la chaine de caractères de la requête en tableau de chaines de caractères
//  ex : "toto;tata;titi" => ["toto","tata","titi"] 
char **requestToArray(char* requestString){
  char* token = strtok(requestString, ";");

  char** requestArray = (char**)malloc(sizeof(char*));
  requestArray[0] = token;
  
  int i=0;
  while(token != NULL){
    token=strtok(NULL, ";");
    i++;
    char** newStringArray = (char**)malloc((i+1)*sizeof(char*));
    memcpy(newStringArray, requestArray, i*sizeof(char*));
    newStringArray[i]=token;
    requestArray = newStringArray;
  }

  return requestArray;
}


uint8_t **getData(uint8_t *lenGreenhouseArray, uint8_t **greenhouseArray, uint8_t **lenGreenhouseData, uint8_t measureType){
  *greenhouseArray = getGreenhouseTable(lenGreenhouseArray);
  *lenGreenhouseData = (uint8_t*) malloc(*lenGreenhouseArray * sizeof(uint8_t));
  
  uint8_t **greenhouseTableData = (uint8_t**)malloc(nbDestinataires * sizeof(uint8_t*));

  for(uint8_t i=0; i<*lenGreenhouseArray; i++){
    uint8_t *report = (uint8_t*)malloc(nbReport * sizeof(uint8_t));
    for(int8_t j=nbReport-1; j>=0; j--){
      //Serial.println("[TestLight] " + String(reportingTab[greenhouseId][i][reportingLuminosite]));
      report[j] = reportingTab[i][j][measureType];
    }
    greenhouseTableData[i] = report;
  }
  
  return greenhouseTableData;
}

// todo : intégrer
uint8_t *getGreenhouseTable(uint8_t *len){
  uint8_t *fakeGreenhouses = (uint8_t*) malloc(3 * sizeof(uint8_t));
  *len = (uint8_t*)malloc(sizeof(uint8_t));
  *len = 2;

  fakeGreenhouses[0] = 1;
  fakeGreenhouses[1] = 2;

  return fakeGreenhouses;
}

// todo : intégrer
char setLight(uint8_t greenhouseId, uint8_t value){
  return SUCCESS;
}

// todo : intégrer
char setFanSpeed(uint8_t greenhouseId, uint8_t value){
  return FAILURE;
}

// renvoie une chaine de caractère représentant le tableau en paramètres
String toString(uint8_t *requestArray, uint8_t lenRequestArray){
  String result = "";

  for(int8_t i=lenRequestArray-1; i>=0; i--){
    result.concat((String)requestArray[i]);

    if(i!=0){
      result.concat(";");
    }
  }

  return result;
}
