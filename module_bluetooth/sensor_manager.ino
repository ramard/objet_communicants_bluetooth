#include "DHT.h"
#include <Servo.h>
DHT dht(DHT_PIN, DHT11);

Servo myservo;

// Variables
int TemperatureThreshold = 27;
int LightThreshold = 800;
int HumidityThreshold = 75;

//////////////////////////
//////////////////////////
/////SPECIFICS SETUP//////
//////////////////////////
//////////////////////////

extern void setupDHT(){
  // Initialisation du capteur de température / humidité
  dht.begin();
}

extern void setupServo() {
  // Initialisation du servo moteur
  myservo.attach(SERVO_PIN); 
  myservo.write(SERVO_INIT_DEG);
}

extern void setupDC() {
  // Initialisation du DC moteur (ventilateur)
  pinMode(DC_ENABLE, OUTPUT);
  pinMode(DC_DIR_A, OUTPUT);
  pinMode(DC_DIR_B, OUTPUT);
  digitalWrite(DC_DIR_A, HIGH); //one way
  digitalWrite(DC_DIR_B, LOW);
}

extern void setupLight(){
  pinMode(LED_PIN, OUTPUT);
  turnOffLight();
}

//////////////////////////
//////////////////////////
/////SPECIFICS LOOPS//////
//////////////////////////
//////////////////////////

extern void loopShowSensorsState(){
  // Montre les valeurs des capteurs
  Serial.print(F("CAPTEURS "));
  Serial.print(F("Humidity: "));
  Serial.print(getHumidityValue());
  Serial.print(F("%  Temperature: "));
  Serial.print(getTemperatureValue());
  Serial.print(F("°C Photocell: "));
  Serial.println(getPhotocellValue());
  delay(1000);
}


extern void loopShowThresholdState(){
  // Montre les valeurs des seuils
  Serial.print(F("SEUILS "));
  Serial.print(F("Humidity: "));
  Serial.print(HumidityThreshold);
  Serial.print(F("%  Temperature: "));
  Serial.print(TemperatureThreshold);
  Serial.print(F("°C Photocell: "));
  Serial.println(LightThreshold);
  delay(1000);
}


extern void loopHumidity(){
  if(getHumidityValue() < HumidityThreshold){
    Serial.println("Ouverture de l'électrovanne");
    myservo.write(180);
    delay(500);
  }else{
    Serial.println("Fermeture de l'électrovanne");
    myservo.write(0);
    delay(500);
  }
}

extern void loopTemperature(){
  if(getTemperatureValue() > TemperatureThreshold){
    Serial.println("Activation ventillateur");
    runDC();
    delay(500);
  }else{
    Serial.println("Désactivation ventillateur");
    stopDC();
    delay(500);
  }
}

extern void loopLight(){
  if(getPhotocellValue() < LightThreshold){
    Serial.println("Allumage lumière");
    turnOnLight();
    delay(500);
  }else{
    Serial.println("Éteignage lumière");
    turnOffLight();
    delay(500);
  }
}

//////////////////////////
//////////////////////////
///////LED CONTROLS///////
//////////////////////////
//////////////////////////

void turnOnLight(){
  digitalWrite(LED_PIN, HIGH);
}

void turnOffLight(){
  digitalWrite(LED_PIN, LOW);
}

//////////////////////////
//////////////////////////
///////DC CONTROLS////////
//////////////////////////
//////////////////////////

void runDC(){
    analogWrite(DC_ENABLE, 255);
}

void stopDC(){
    analogWrite(DC_ENABLE, 0);
}

void trigoModeDC(){
  digitalWrite(DC_DIR_A, HIGH); //one way
  digitalWrite(DC_DIR_B, LOW);
}

void antiTrigoModeDC(){
  digitalWrite(DC_DIR_A, LOW); //one way
  digitalWrite(DC_DIR_B, HIGH);
}

//////////////////////////
//////////////////////////
////GET SENSORS VALUE/////
//////////////////////////
//////////////////////////

extern float getPhotocellValue(){
  return analogRead(PHOTO_PIN);
}

extern int getHumidityValue(){
  return dht.readHumidity();
}

extern int getTemperatureValue(){
  return dht.readTemperature();
}
