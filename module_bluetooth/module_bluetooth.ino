#include <SoftwareSerial.h>
#include <stdlib.h>

/*
 *  A CHANGER EN FONCTION DE LA CARTE
 */
#define MONNUMERO 1

/*
 * PIN CONFIG
 */
#define RxD 9
#define TxD 8
//#define green_led 4
//#define red_led 5
#define gResetPin 11 // cable branché sur le neutre du module bluetooth. Permet de l'éteindre en envoyant 5v ou de l'allumer en branchant sur le ground
#define gKeyPin 12 // pin permettant d'activer le mode configuration du module bluetooth
#define photoCellPin 1 // pin utiliser pour récupérer les informations de la photo-resistance


// Définitions des pins
#define PHOTO_PIN A0  

#define DHT_PIN 2   
#define DC_DIR_A 3
#define DC_DIR_B 4
#define DC_ENABLE 5
#define LED_PIN 6
#define SERVO_PIN 7

#define SERVO_INIT_DEG 0

/*
 * TIME CONFIG
 */
#define bt_tempo 100
#define bt_timeout_receive 4000
#define temp_refresh_time 500
#define restart_delay 2000
#define howLongToWait 65000
#define waitFactor 34567

/*
 * REPORTING CONFIG
 */
#define nbDestinataires 2
#define nbConfigValue 4
#define nbReportingValue 4 // nombre valeurs de reporting sauvegardés // timestamp | luminosité | hydrométrie | température
#define nbReport 10 // nombres de valeurs historique gardé
#define timeReport 10 // secondes entre chaque report

/*
 * REPORTING VALUE
 */
#define reportingTimestamp 0
#define reportingLuminosite 1
#define reportingHydrometrie 2
#define reportingTemperature 3

// COMMUNICATION PC
#define BUFFER_SIZE 64
#define TEMPERATURE 'T'
#define LIGHT 'L'
#define FAN_SPEED 'F'
#define HUMIDITY 'H'
#define ALL 'A'
#define READ 'R'
#define WRITE 'W'
#define SUCCESS 'S'
#define FAILURE 'F'
#define END "END"
#define ACK "ACK"

/*
 * VARIABLE INIT
 */
export SoftwareSerial BTSerie(RxD,TxD);
String destinataires[nbDestinataires] = {"98d3,51,fe219b","98d3,51,fe21d4"}; // cyrille : 98d3:51:fe219b  gurvan : 98d3:51:fe21d4
uint8_t testLed[nbDestinataires]={1,2};
export uint8_t configTab[nbDestinataires][nbConfigValue];
export uint8_t reportingTab[nbDestinataires][nbReport][nbReportingValue]; //  
uint8_t configVersion;

char inData[BUFFER_SIZE];
char inChar=-1;
int count=0;
int i=0;

extern int TemperatureThreshold;
extern int LightThreshold;
extern int HumidityThreshold;

// Fonction appelé pour redémarrer le module bluetooth
void BtReset(void){
  Serial.println("BtReset");    
  BTSerie.flush();
  delay(temp_refresh_time);
  digitalWrite(gResetPin, LOW);  
  delay(restart_delay);
  digitalWrite(gResetPin, HIGH);
  delay(restart_delay);
}

// Fonction appelé pour activer ou désactiver le mode configuration du module bluetooth
void BtSetCmdMode(uint8_t i_cmdMode){
  Serial.println("BtSetCmdMode ");    
  digitalWrite(gKeyPin, ((1 == i_cmdMode)?HIGH:LOW)); // si 1 est passé en paramètre de la fonction, active le mode configuration sinon désactive
  BtReset();
  BTSerie.begin(38400);
  delay(temp_refresh_time);
}

// fonction permettant de définir le role du module bluetooth( maitre ou esclave)
void BtSetupRole(uint8_t i_isMaster, uint8_t destinataire){
  BtSetCmdMode(1);
  /*
  BTSerie.println("AT+ORGL ");
  BTSerie.println("AT+UART=38400,0,0, ");
  BtSetCmdMode(1);
  BTSerie.println("AT+CMODE=0");
  BTSerie.println("AT+PSWD=1234");
  */
//  BTSerie.flush();
  if(0 == i_isMaster){
    BTSerie.println("AT+ROLE=0 ");
    Serial.println("Set as Slave ");
    delay(temp_refresh_time);
  } else {
    BTSerie.println("AT+ROLE=1 ");
    BtSetCmdMode(1);
    BTSerie.println("AT+CMODE=0");
    BTSerie.println("AT+BIND=" + destinataires[destinataire]);
    Serial.println("AT+BIND=" + destinataires[destinataire]);
    Serial.println("Set as Master ");
    delay(temp_refresh_time);
  }
//  BTSerie.flush();
  BtSetCmdMode(0);
}



void tabZero()
{
  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbConfigValue;j++){
        configTab[i][j]=i+j+MONNUMERO;
    }
  }
  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbReport;j++){
      for(uint8_t k=0; k<nbReportingValue;k++){
        reportingTab[i][j][k]=0;
      }
    }
  }
}

void sendConfig()
{
  configTab[nbDestinataires][nbConfigValue];
  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbConfigValue;j++){
        BTSerie.write(configTab[i][j]);
    }
  }
}

void receiveConfig()
{
  Serial.print("receive Config Version : ");
  boolean received = false;
  boolean save = false;
  uint8_t newVersion;
  unsigned long startedWaiting = millis();
  while (received==false && millis() - startedWaiting <= bt_timeout_receive )
  {
      if (BTSerie.available())
      {
      newVersion = BTSerie.read();
      Serial.println(newVersion);
      received=true;
      }
  }

  if(newVersion>configVersion)
  {
    save=true;
    configVersion=newVersion;
    Serial.println("Save Config");
  }
 
  startedWaiting = millis();
  uint8_t i=0;
  uint8_t j=0;
  // Receive data
  uint8_t rec=0;
  Serial.println("receive Config");

  while ((i<nbDestinataires) && (millis() - startedWaiting <= bt_timeout_receive ) )
  {
      if (BTSerie.available())
      {
        rec = BTSerie.read();
        Serial.print(rec);
        if (save==true)
        {
          configTab[i][j]=rec;

          //mise à jour des seuils courants
          if(i == MONNUMERO){
            if(j == reportingLuminosite){
              TemperatureThreshold = rec;
            }
            else if(j == reportingHydrometrie){
              HumidityThreshold = rec;
            }
            else if(j == reportingTemperature){
              TemperatureThreshold = rec;
            }
          }
        }
        if(j==nbDestinataires-1)
        {
          i++;
          j=0;
          Serial.print(" ## ");           
        }
        else
        {
          j++;
          Serial.print(",");
        }
      }
  }
}

/*
 * pour chaque ligne (nb destinataire * nbreport)
 *  master envoie numero destinataire + numero timestamp
 *  si slave a la ligne
 *   slave envoie 0
 *  si slave n'a pas la ligne
 *    slave envoie 1
 *    master envoie les données une à une pour remplir le tableau
 */
void sendReport(){
  Serial.println("send report ");

  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbReport;j++){
          for(uint8_t k=0; k<nbReportingValue;k++){
                BTSerie.write(reportingTab[i][j][k]);
          }
    }
    delay(bt_tempo);
  }
}

void receiveReport(){
  uint8_t i=0;
  uint8_t j=0;
  uint8_t k=0;
  uint8_t rec=0;

  uint8_t newReportingTab[nbDestinataires][nbReport][nbReportingValue];
  Serial.println("receiveReport ");
  unsigned long startedWaiting = millis();

  // Receive data
  while ((i<nbDestinataires) && (millis() - startedWaiting <= bt_timeout_receive ) )
  {
      if (BTSerie.available())
      {
        rec=BTSerie.read();
        newReportingTab[i][j][k]=rec;
        Serial.print(rec);           
        if(k==nbReportingValue-1)
        {
          j++;
          k=0;
          Serial.print(" ## ");           
        }
        else
        {
          k++;
          Serial.print(',');           

        }
        if(j==nbReport)
        {
          i++;
          j=0;
          Serial.print(" ... ");           
        }
      }
  }
      Serial.println("Report received ");
  unsigned long newTimestamp;
  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbReport;j++){
        newTimestamp = newReportingTab[i][j][reportingTimestamp];
        if(reportingTab[i][j][reportingTimestamp]<newTimestamp || reportingTab[i][j][reportingTimestamp]>255-nbReport )
        {
           for(uint8_t k=0; k<nbReportingValue;k++){
              reportingTab[i][j][k]= newReportingTab[i][j][k];
           }
        }
    }
  }
}

/*
 * PROTOCOLE DIALOGUE ARDUINO
 * 1. CONFIG
 *  master envoie à slave son num
 *  si num >
 *    slave envoie 1
 *    master envoie tous les bytes
 *  si num <
 *    slave envoie 0
 * 2. REPORT
 * pour chaque ligne (nb destinataire * nbreport)
 *  master envoie numero destinataire + numero timestamp
 *  si slave a la ligne
 *   slave envoie 0
 *  si slave n'a pas la ligne
 *    slave envoie 1
 *    master envoie les données une à une pour remplir le tableau
 *
 */
void arduinoSlaveDialogue(){
  receiveConfig();
  receiveReport();
}

void arduinoMasterDialogue(){
  delay(bt_tempo);
  BTSerie.write(configVersion);
  delay(bt_tempo);
  sendConfig();
  delay(bt_tempo);
  sendReport();
}

/*
 * Remplir le report à la ligne la plus vieille en incrémentant le compteur de 1
 */
void remplirReport()
{
  Serial.println("Remplir Report");
  uint8_t minTime=255;
  uint8_t maxTime=0;
  uint8_t iMin=0;
        for(uint8_t i=nbReport; i>1;i--){
          if(reportingTab[MONNUMERO][i-1][reportingTimestamp]<minTime){
            minTime=reportingTab[MONNUMERO][i-1][reportingTimestamp];
            iMin=i-1;
          }
          if(reportingTab[MONNUMERO][i-1][reportingTimestamp]>maxTime){
            maxTime=reportingTab[MONNUMERO][i-1][reportingTimestamp];
          }
        }
      for(uint8_t k=1; k<nbReportingValue;k++){
        reportingTab[MONNUMERO][iMin][k]=MONNUMERO + 3;
      }
      reportingTab[MONNUMERO][iMin][reportingTimestamp]=maxTime+1;
      
      // Read the sensor pin
      uint8_t photoCellValue = analogRead(photoCellPin);
      Serial.println("Valeur de la photocell : ");
      Serial.println(photoCellValue);
      reportingTab[MONNUMERO][iMin][reportingLuminosite]=photoCellValue;

      // Read the humidity pin
      uint8_t humidityValue = getHumidityValue();
      Serial.println("Valeur de l'humidité : ");
      Serial.println(humidityValue);
      reportingTab[MONNUMERO][iMin][reportingHydrometrie]=humidityValue;

      // Read the temperature pin
      uint8_t temperatureValue = getTemperatureValue();
      Serial.println("Valeur de la température : ");
      Serial.println(temperatureValue);
      reportingTab[MONNUMERO][iMin][reportingTemperature]=temperatureValue;

}

void propager(uint8_t c){
  for (uint8_t i = 0; i<nbDestinataires;i++)
  {
    if(i!=MONNUMERO)
    {
       BtSetupRole(1,i);
       delay(temp_refresh_time);
       Serial.print("Transmission ");
       Serial.print(c);
       Serial.println(" ");

       char value;
       itoa (c,value,10);
       
       BTSerie.write(value);
       //while (BTSerie.available()) { Serial.println(BTSerie.read()); }
       c=3;
       delay(bt_tempo);
       BTSerie.write('3');
       arduinoMasterDialogue();
    }
  }
  BtSetupRole(0,0);
}

void printTab(){
  Serial.print("config version : ");
  Serial.println(configVersion);
  Serial.println("config :");
  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbConfigValue;j++){
       Serial.print(configTab[i][j]);
       Serial.print(',');
    }
    Serial.print(" || ");
  }
  Serial.println(" ");
  Serial.println("report :");

  for(uint8_t i=0; i<nbDestinataires;i++){
    for(uint8_t j=0; j<nbReport;j++){
      for(uint8_t k=0; k<nbReportingValue;k++){
        Serial.print(reportingTab[i][j][k]);
        Serial.print(',');
      }
      Serial.print(" || ");
    }
    Serial.print(" ### ");
  }
  Serial.println(" ");

  Serial.println("end");
}

void setup() {
  
  pinMode(RxD,INPUT);
  pinMode(TxD,OUTPUT);
  Serial.begin(9600);
  Serial.println("Projet embarque serre");

  // BLUETOOTH
  pinMode(gResetPin, OUTPUT);
  digitalWrite(gResetPin, LOW);
  pinMode(gKeyPin, OUTPUT);
  digitalWrite(gKeyPin, LOW);
 
  BtSetupRole(0,0);

  // Setup sensor
  setupDHT();
  setupServo();
  setupDC();
  setupLight();
 
  // LED
//  pinMode(green_led, OUTPUT);
//  digitalWrite(green_led, HIGH);
//  pinMode(red_led, OUTPUT);
//  digitalWrite(red_led, HIGH);
  configVersion=MONNUMERO;
  tabZero(); 
}

void loop() {
 Serial.println("Nouvelle boucle ");
//  digitalWrite(green_led, HIGH);
//  digitalWrite(red_led, HIGH);
  
  uint8_t c=0;
  unsigned long startedWaiting = millis();
//  BTSerie.flush();

  // Receive data
  while (millis() - startedWaiting <= (howLongToWait + waitFactor*MONNUMERO )) {
    listen(manageComputerRequest);
  }

//  digitalWrite(green_led, LOW);
//  digitalWrite(red_led, LOW);
  //propager(testLed[MONNUMERO]);
  remplirReport();
  printTab();
}

void loopSensor(){
  loopShowSensorsState();
  loopShowThresholdState();
  loopHumidity();
  loopTemperature();
  loopLight();  
}

void listen(void (*callback)(char*)) {
  //This will prevent bufferoverrun errors
  byte byte_count=BTSerie.available();
  if(byte_count)
  {
    Serial.println("IncomingData");
    int first_bytes = byte_count;
    int remaining_bytes = 0;
    if(first_bytes >= BUFFER_SIZE-1)
    {
      remaining_bytes = byte_count - (BUFFER_SIZE-1);
    }
    for(i=0; i<first_bytes; i++)
    {
      inChar = BTSerie.read();
      inData[i] = inChar;
    }
    inData[i]='\0';
    for(i=0; i<remaining_bytes; i++) //This burns off any remaining bytes that the buffer can't handle
    {
      inChar = BTSerie.read();
    }

    if(inData[0] == '1') {
//      digitalWrite(green_led, LOW);
//      digitalWrite(red_led, HIGH);
    }
    else if(inData[0] == '2') {
//      digitalWrite(green_led, HIGH);
//      digitalWrite(red_led, LOW);
    }
    else if(inData[0] == '3') {
      Serial.println("Arduino dialogue ");
      arduinoSlaveDialogue();
    }
    else{
      Serial.println(inData);
      callback(inData); 
    }
  }
  delay(100);
}
